/** \file coords_vector_transformations.hpp
 * \ingroup fibres_direction
 * \brief Coordinate and vector transformations for cylindrical and spherical coordinate systems
 */

#ifndef FIBRESDIRECTIONDEF
#define FIBRESDIRECTIONDEF

/** \ingroup fibres_directions
* Transform cartesian coordinates to cylindrical coordinates:
* \f[ \begin{bmatrix}
*      \theta \\[0.3em]
*      \phi \\[0.3em]
*      r
*  \end{bmatrix}
*  =
*  \begin{bmatrix}
*      \arctan(x,y) \\[0.3em]
*      z \\[0.3em]
*      \sqrt[2]{x^2+y^2}
*  \end{bmatrix} \f]
*
* Transform cylindrical vector to cartesian vector:
* \f[ J_{cyl} = \begin{bmatrix}
*      -r\sin{\theta} & 0 & 0 \\[0.2em]
*      r\cos{\theta} & 0 & 0 \\[0.2em]
*      0 & 1 & 0
*  \end{bmatrix} \f]
*
* Transform cartesian coordinates to spherical coordinates:
* \f[ \begin{bmatrix}
*      \theta \\[0.3em]
*      \phi \\[0.3em]
*      r
*  \end{bmatrix}
*  =
*  \begin{bmatrix}
*      \arctan(x,y) \\[0.3em]
*       \arctan(z,r) \\[0.3em]
*      \sqrt[2]{x^2+y^2+z^2}
*  \end{bmatrix} \f]
*
* Transform spherical vector to cartesian vector:
* \f[ J_{sph} = \begin{bmatrix}
*    -r\cos(\phi)\sin{\theta} & -r\cos(\theta)\sin(\phi) & 0 \\[0.2em]
*    r\cos{\theta}\cos(\phi) & -r\sin(\theta)\sin(\phi) & 0 \\[0.2em]
*    0 & r\cos(\phi) & 0
*  \end{bmatrix} \f]
*/

struct CoordsVectorTransformations
{
  void Coords_CartesianToCylindrical(double* P_coords_c, double* P_coords_cyl);

  void Vector_CylindricalToCartesian(double* P_coords_cyl, double* P_V_cyl, double* P_V_c);

  void Coords_CartesianToSpherical(double* P_coords_c, double* P_coords_sph);

  void Vector_SphericalToCartesian(double* P_coords_sph, double* P_V_sph, double* P_V_c);
};

#endif
