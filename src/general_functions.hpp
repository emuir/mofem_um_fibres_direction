#ifndef GENERALFUNCTIONS
#define GENERALFUNCTIONS

struct GeneralFunctions
{
  void PrintArray(double* P_Array, int M, int N);

  void RoundAtPrec(int Prec, double* P_Array, int M, int N);

  void Normalise(double* P_Array, int M);

  void IfNaNAssign0(double* P_Array, int M);
};

#endif
