#include "linear_vector_field_function.hpp"

void VectorField::returnLinearVector(double* P_coords_vs, double* P_V1_vs, double* P_V2_vs)
{
  //Family 1
  P_V1_vs[0] = 1;
  P_V1_vs[1] = 1;
  P_V1_vs[2] = 0;
  //Family 2
  P_V2_vs[0] = -1;
  P_V2_vs[1] = 1;
  P_V2_vs[2] = 0;
}
