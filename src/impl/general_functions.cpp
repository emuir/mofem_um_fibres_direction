#include <iostream>
#include <cmath>
#include "general_functions.hpp"

void GeneralFunctions::PrintArray(double* P_Array, int M, int N)
{
  for(int it=0;it<(M*N);it++) {
    std::cout << P_Array[it] << std::endl;
  }
}

void GeneralFunctions::RoundAtPrec(int Prec, double* P_Array, int M, int N)
{
  for(int it=0;it<(M*N);it++) {
    double val = *(P_Array+it);
    *(P_Array+it) = (round((val*pow(10,Prec))))/pow(10,Prec);
  }
}

void GeneralFunctions::Normalise(double* P_Array, int M)
{
  double mod = 0;
  for(int it=0;it<M;it++) {
    mod += pow(P_Array[it],2);
  }
  mod = sqrt(mod);

  P_Array[0] = (1/mod)*P_Array[0];
  P_Array[1] = (1/mod)*P_Array[1];
  P_Array[2] = (1/mod)*P_Array[2];
}

void GeneralFunctions::IfNaNAssign0(double* P_Array, int M)
{
  for(int it=0;it<M;it++) {
    //Check for NaN
    if (P_Array[it] != P_Array[it]) {
      std::cout << "NaN detected and replaced with 0" << std::endl;
      P_Array[it] = 0;
    }
  }
}
