#include <MoFEM.hpp>
using namespace MoFEM;

#include <string>

#include "coords_vector_transformations.hpp"

void CoordsVectorTransformations::Coords_CartesianToCylindrical(double* P_coords_c, double* P_coords_cyl)
{
  double x,y,z,r,theta,phi;

  x = P_coords_c[0];
  y = P_coords_c[1];
  z = P_coords_c[2];

  r = sqrt(pow(x,2)+pow(y,2));
  theta = atan2(y,x);
  phi = z;

  P_coords_cyl[0] = theta;
  P_coords_cyl[1] = phi;
  P_coords_cyl[2] = r;
}

//Vector Cylindrical to Rectangular coordinate system
void CoordsVectorTransformations::Vector_CylindricalToCartesian(double* P_coords_cyl, double* P_V_cyl, double* P_V_c)
{
  double theta,r;
  theta = P_coords_cyl[0];
  r = P_coords_cyl[2];
  //Transformation Matrix
  double trans_m[9] = {-r*sin(theta),r*cos(theta),0,0,0,1,0,0,0};

  //Rect Vector
  cblas_dgemv(CblasRowMajor, CblasTrans, 3,3,1, &trans_m[0], 3, &P_V_cyl[0], 1,0, &P_V_c[0], 1);
}

//Coords Rectangular to Spherical coordinate system
void CoordsVectorTransformations::Coords_CartesianToSpherical(double* P_coords_c, double* P_coords_sph)
{
  double x,y,z,r,theta,phi;

  x = P_coords_c[0];
  y = P_coords_c[1];
  z = P_coords_c[2];

  r = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
  theta = atan2(y,x);
  phi = asin(z/r);

  P_coords_sph[0] = theta;
  P_coords_sph[1] = phi;
  P_coords_sph[2] = r;
}

//Vector Spherical to Rectangular coordinate system
void CoordsVectorTransformations::Vector_SphericalToCartesian(double* P_coords_sph, double* P_V_sph, double* P_V_c)
{
  double theta,phi,r;

  theta = P_coords_sph[0];
  phi = P_coords_sph[1];
  r = P_coords_sph[2];
  //Transformation Matrix
  double trans_m[9] = {-r*cos(phi)*sin(theta),r*cos(theta)*cos(phi),0,-r*cos(theta)*sin(phi),-r*sin(phi)*sin(theta),r*cos(phi),0,0,0};

  //Rect Vector
  cblas_dgemv(CblasRowMajor, CblasTrans, 3,3,1, &trans_m[0], 3, &P_V_sph[0], 1,0, &P_V_c[0], 1);
}
