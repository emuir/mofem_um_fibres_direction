#ifndef FIBREFUNCTION
#define FIBREFUNCTION

struct VectorField{
  void returnLinearVector(double* P_coords_vs, double* P_V1_vs, double* P_V2_vs);
};

#endif
