/** \file apply_vector_field.cpp
 * \ingroup fibres_direction
 * \brief Apply vector field
 */
#include <MoFEM.hpp>
using namespace MoFEM;
#include "linear_vector_field_function.hpp"
#include "coords_vector_transformations.hpp"
#include "general_functions.hpp"

ErrorCode rval;
PetscErrorCode ierr;

GeneralFunctions general_functions;
VectorField linear_vector_field;
CoordsVectorTransformations coords_vector_transformations;

Tag th_fibreDirection1;
Tag th_fibreDirection2;

static char help[] = "...\n\n";

int main(int argc, char *argv[])
{
  PetscInitialize(&argc,&argv,(char *)0,help);

  moab::Core mb_instance;
  Interface& moab = mb_instance;

  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  //load file
  const char *option;
  option = "";
  rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ(rval);

  MoFEM::Core core(moab);
  FieldInterface& m_field = core;

  //ref meshset ref level 0
  ierr = m_field.seed_ref_level_3D(0,BitRefLevel().set(0)); CHKERRQ(ierr);
  vector<BitRefLevel> bit_levels;
  bit_levels.push_back(BitRefLevel().set(0));
  BitRefLevel problem_bit_level;

  problem_bit_level = bit_levels.back();

  Range CubitSideSets_meshsets;
  ierr = m_field.get_cubit_meshsets(SIDESET,CubitSideSets_meshsets); CHKERRQ(ierr);

  //Fibre field
  ierr = m_field.add_field("FIBRES_DIRECTION_1",H1,3); CHKERRQ(ierr);
  ierr = m_field.add_field("FIBRES_DIRECTION_2",H1,3); CHKERRQ(ierr);

  ierr = m_field.add_ents_to_field_by_TETs(0,"FIBRES_DIRECTION_1"); CHKERRQ(ierr);
  ierr = m_field.add_ents_to_field_by_TETs(0,"FIBRES_DIRECTION_2"); CHKERRQ(ierr);

  int order = 1;
  ierr = m_field.set_field_order(0,MBVERTEX,"FIBRES_DIRECTION_1",1); CHKERRQ(ierr);
  ierr = m_field.set_field_order(0,MBVERTEX,"FIBRES_DIRECTION_2",1); CHKERRQ(ierr);

  ierr = m_field.build_fields(); CHKERRQ(ierr);


  //Read parameters from line command
  //
  //Meshfile

  //================================================
  //Transformation type
  flg = PETSC_TRUE;
  char transformation_type[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-trans_type", transformation_type,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"ERROR:Supported transformations:rect_rect, rect_cyl, rect_sph");
  }
  string transformation_type_str = transformation_type;

  //Cartesian
  double coords_c[3];
  double V1_c[3], V2_c[3];
  //Vector space
  double coords_vs[3];
  double V1_vs[3], V2_vs[3];

  //Transformation type
  flg = PETSC_TRUE;
  char file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-file_field_input", file_name,255,&flg); CHKERRQ(ierr);
  string file_name_str = string(file_name);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,1,"ERROR:Supported command line input y or n");
  }
  else if (file_name_str != "none"){
    //line number counter
    int lnn = 0;
    ifstream infile;
    infile.open(file_name);
    string a,b;
    //c stores the vector component direction
    double c;
    while (infile >> a >> b >> c)
    {
      lnn++;
      if (lnn < 4) {
        V1_vs[lnn-1] = c;
      }
      else {
        V2_vs[lnn-4] = c;
      }
    }
    //check if file has been read
    if (lnn > 0){
  cout << "Fibre Directions from file:" << endl;
  cout << "V1" << endl;
  general_functions.PrintArray(&V1_vs[0],1,3);
  cout << "V2" << endl;
  general_functions.PrintArray(&V2_vs[0],1,3);
  cout << " " << endl;
    }
  }


  //Transformations
  Range::iterator rit;
  for( _IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field,"FIBRES_DIRECTION_1",dof)) {
    EntityHandle node = (*dof)->get_ent();

    //Return coordinates for a specific node
    m_field.get_moab().get_coords(&node,1,coords_c);
    int rank = (*dof)->get_dof_rank();

    if (transformation_type_str == "cart_cyl"){
      coords_vector_transformations.Coords_CartesianToCylindrical(&coords_c[0], &coords_vs[0]);
    }
    else if (transformation_type_str == "cart_sph"){
      coords_vector_transformations.Coords_CartesianToSpherical(&coords_c[0], &coords_vs[0]);
    }

    if (file_name_str == "none"){
      linear_vector_field.returnLinearVector(&coords_vs[0], &V1_vs[0], &V2_vs[0]);
    }

    if (transformation_type_str == "cart_cart") {
      V1_c[0] = V1_vs[0]; V1_c[1] = V1_vs[1]; V1_c[2] = V1_vs[2];
    }
    else if (transformation_type_str == "cart_cyl"){
      coords_vector_transformations.Vector_CylindricalToCartesian(&coords_vs[0], &V1_vs[0], &V1_c[0]);
    }
    else if (transformation_type_str == "cart_sph"){
      coords_vector_transformations.Vector_SphericalToCartesian(&coords_vs[0], &V1_vs[0], &V1_c[0]);
    }

    general_functions.Normalise(&V1_c[0],3);
    general_functions.IfNaNAssign0(&V1_c[0],3);

    // std::cout << std::endl << V1_c[0] << " " << V1_c[1] << " " << V1_c[2] << std::endl;

    (*dof)->get_FieldData() = V1_c[rank];
  }

  for( _IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field,"FIBRES_DIRECTION_2",dof)) {
    EntityHandle node = (*dof)->get_ent();

    m_field.get_moab().get_coords(&node,1,coords_c);
    int rank = (*dof)->get_dof_rank();

    if (transformation_type_str == "cart_cyl"){
      coords_vector_transformations.Coords_CartesianToCylindrical(&coords_c[0], &coords_vs[0]);
    }
    else if (transformation_type_str == "cart_sph"){
      coords_vector_transformations.Coords_CartesianToSpherical(&coords_c[0], &coords_vs[0]);
    }

    if (file_name_str == "none"){
      linear_vector_field.returnLinearVector(&coords_vs[0], &V1_vs[0], &V2_vs[0]);
    }

    if (transformation_type_str == "cart_cart") {
      V2_c[0] = V2_vs[0]; V2_c[1] = V2_vs[1]; V2_c[2] = V2_vs[2];
    }
    else if (transformation_type_str == "cart_cyl"){
      coords_vector_transformations.Vector_CylindricalToCartesian(&coords_vs[0], &V2_vs[0], &V2_c[0]);
    }
    else if (transformation_type_str == "cart_sph"){
      coords_vector_transformations.Vector_SphericalToCartesian(&coords_vs[0], &V2_vs[0], &V2_c[0]);
    }

    general_functions.Normalise(&V2_c[0],3);
    general_functions.IfNaNAssign0(&V2_c[0],3);

    (*dof)->get_FieldData() = V2_c[rank];
  }

  if (file_name_str == "none") {
  cout << "Fibre Directions from function:" << endl;
  cout << "V1" << endl;
  general_functions.PrintArray(&V1_vs[0],1,3);
  cout << "V2" << endl;
  general_functions.PrintArray(&V2_vs[0],1,3);
  cout << " " << endl;
}

  cout << "Fibres direction added to mesh" << endl;

  rval = moab.write_file("fibres.h5m"); CHKERRQ(rval);

  ierr = PetscFinalize(); CHKERRQ(ierr);
}

/***************************************************************************//**
 * \defgroup fibres_direction Fibres Direction
 * \ingroup user_modules
 ******************************************************************************/
